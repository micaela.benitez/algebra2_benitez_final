﻿using System.Collections.Generic;
using UnityEngine;
using CustomMath;
using FrustumCullingScript;

namespace CubeScript
{
    /// Se calcula cada vertice del cubo y se chequea que esten todos dentro del frustum culling.
    /// Con que uno ya este dentro, se dibuja todo el objeto.

    public class Cube : MonoBehaviour
    {
        bool inside = false;
        private MeshRenderer cubeMesh;
        private List<Vec3> cubeEdges = new List<Vec3>();

        private void Awake()
        {
            cubeMesh = gameObject.GetComponent<MeshRenderer>();

            for (int i = 0; i < 8; i++)
            {
                cubeEdges.Add(Vec3.Zero);
            }
        }

        private void Start()
        {
            EdgesCalculation();   // Calculo los vertices del cubo
        }

        private void Update()
        {
            CubeInsideRoom();   // Chequeo si el cubo esta dentro de las 6 paredes o no  
        }

        public void EdgesCalculation()
        {
            Vec3 pos = new Vec3(gameObject.transform.position);
            Vec3 scale = new Vec3(gameObject.transform.lossyScale / 2);
            Vec3 up = new Vec3(gameObject.transform.up);
            Vec3 forward = new Vec3(gameObject.transform.forward);
            Vec3 right = new Vec3(gameObject.transform.right);

            for (int i = 0; i < cubeEdges.Count; i++)
            {
                cubeEdges[i] = pos + (up * scale.y);

                if (i == 0 || i == 1 || i == 4 || i == 5) cubeEdges[i] += (forward * scale.z);
                else cubeEdges[i] -= (forward * scale.z);

                if (i % 2 == 0) cubeEdges[i] += (right * scale.x);
                else cubeEdges[i] -= (right * scale.x);
            }
        }

        public void CubeInsideRoom()
        {
            inside = false;
            for (int i = 0; i < cubeEdges.Count; i++)
            {
                if (FrustumCulling.Get().nearPlane.GetSide(cubeEdges[i]) &&
                    FrustumCulling.Get().farPlane.GetSide(cubeEdges[i]) &&
                    FrustumCulling.Get().leftPlane.GetSide(cubeEdges[i]) &&
                    FrustumCulling.Get().rightPlane.GetSide(cubeEdges[i]) &&
                    FrustumCulling.Get().topPlane.GetSide(cubeEdges[i]) &&
                    FrustumCulling.Get().bottomPlane.GetSide(cubeEdges[i]))
                    inside = true;
            }

            if (inside) cubeMesh.enabled = true;
            else cubeMesh.enabled = false;
        }
    }
}