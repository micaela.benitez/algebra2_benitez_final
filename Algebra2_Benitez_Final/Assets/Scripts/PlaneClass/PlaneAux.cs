﻿using System;

namespace CustomMath
{
    public struct PlaneAux : IEquatable<PlaneAux>
    {
        #region Variables
        private Vec3 planeNormal;
        private float planeDistance;

        /// Vector normal del plano
        public Vec3 normal   
        {
            get { return planeNormal; }
            set { planeNormal = value; }
        }

        /// La distancia medida desde el plano al origen, a lo largo de la normal del plano
        public float distance   
        {
            get { return planeDistance; }
            set { planeDistance = value; }
        }

        /// Devuelve una copia del plano que mira en la dirección opuesta
        public PlaneAux flipped   
        {
            get { return new PlaneAux(-planeNormal, -planeDistance); }
        }
        #endregion

        #region Constructors
        /// 2 vectores 3
        public PlaneAux(Vec3 inNormal, Vec3 inPoint)
        {
            planeNormal = Vec3.Normalize(inNormal);
            planeDistance = -Vec3.Dot(planeNormal, inPoint);
        }

        /// La normal y la distancia
        public PlaneAux(Vec3 inNormal, float d)
        {
            planeNormal = Vec3.Normalize(inNormal);
            planeDistance = d;
        }

        /// 3 puntos dentro del plano
        public PlaneAux(Vec3 a, Vec3 b, Vec3 c)
        {
            planeNormal = Vec3.Normalize(Vec3.Cross(b - a, c - a));
            planeDistance = -Vec3.Dot(planeNormal, a);
        }
        #endregion

        #region Functions
        /// Para un punto dado, devuelve el punto más cercano al plano
        /// Se calcula la distancia perpendiular del punto al plano, 
        /// el punto con menor distancia es el punto mas cercano
        public Vec3 ClosestPointOnPlane(Vec3 point)
        {
            float num = Vec3.Dot(planeNormal, point) + planeDistance;
            return point - planeNormal * num;
        }

        /// Hace que el plano mire en la dirección opuesta
        public void Flip()
        {
            planeNormal = -planeNormal;
            planeDistance = -planeDistance;
        }

        /// Distancia de un punto a un plano
        /// Devuelve el modulo de la distancia de un punto al plano
        /// Resultado +: el punto esta del lado que mira el plano
        public float GetDistanceToPoint(Vec3 point)
        {
            return Vec3.Dot(planeNormal, point) + planeDistance;
        }

        /// Devuelve si un punto esta del lado positivo del plano o no
        public bool GetSide(Vec3 point)
        {
            return Vec3.Dot(planeNormal, point) + planeDistance > 0.0f;
        }

        /// Devuelve si hay dos puntos del mismo lado del plano
        public bool SameSide(Vec3 inPt0, Vec3 inPt1)
        {
            float distanceToPoint1 = GetDistanceToPoint(inPt0);
            float distanceToPoint2 = GetDistanceToPoint(inPt1);
            return distanceToPoint1 > 0.0 && distanceToPoint2 > 0.0 || distanceToPoint1 <= 0.0 && distanceToPoint2 <= 0.0;
        }

        /// Establece un plano utilizando tres puntos que se encuentran dentro de él
        public void Set3Points(Vec3 a, Vec3 b, Vec3 c)
        {
            planeNormal = Vec3.Normalize(Vec3.Cross(b - a, c - a));
            planeDistance = -Vec3.Dot(planeNormal, a);
        }

        /// Establece un plano utilizando un punto que se encuentra dentro de él junto con una normal 
        /// para orientarlo, la normal debe estar normalizada
        public void SetNormalAndPosition(Vec3 inNormal, Vec3 inPoint)
        {
            planeNormal = Vec3.Normalize(inNormal);
            planeDistance = Vec3.Dot(inNormal, inPoint);
        }

        /// Devuelve una copia del plano dado, que se mueve en el espacio por una traslación dada. Al 
        /// plano se lo desplaza sumandole el producto punto de la normal y el nuevo vector al que se lo quiere mover
        public void Translate(Vec3 translation)
        {
            planeDistance += Vec3.Dot(planeNormal, translation);
        }
        #endregion

        #region Internals
        public override bool Equals(object other)
        {
            if (!(other is PlaneAux)) return false;
            return Equals((PlaneAux)other);
        }
        public bool Equals(PlaneAux other)
        {
            return (planeNormal == other.normal && planeDistance == other.distance);
        }
        public override int GetHashCode()
        {
            return planeNormal.GetHashCode() ^ (planeDistance.GetHashCode() << 2);
        }
        public override string ToString()
        {
            return "(Normal:(" + planeNormal.x + "," + planeNormal.y + "," + planeNormal.z + ", Distance:" + planeDistance;
        }
        #endregion
    }
}
