﻿using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;
using CustomMath;

public class QuaternionExercises : MonoBehaviour
{
    public enum Ejercicios { uno, dos, tres };
    public Ejercicios ejercicio;
    public float angle;

    private Vector3 vecEjer1;
    private List<Vector3> vecEjer2 = new List<Vector3>();
    private List<Vector3> vecEjer3 = new List<Vector3>();

    private void Start()
    {
        VectorDebugger.EnableCoordinates();

        vecEjer1 = new Vector3(10, 0, 0);

        vecEjer2.Add(new Vector3(10, 0, 0));
        vecEjer2.Add(new Vector3(10, 10, 0));
        vecEjer2.Add(new Vector3(20, 10, 0));

        vecEjer3.Add(new Vector3(10, 0, 0));
        vecEjer3.Add(new Vector3(10, 10, 0));
        vecEjer3.Add(new Vector3(20, 10, 0));
        vecEjer3.Add(new Vector3(20, 20, 0));

        VectorDebugger.AddVector(vecEjer1, "vector1");
        VectorDebugger.UpdateColor("vector1", Color.green);

        VectorDebugger.AddVectorsSecuence(vecEjer2, false, "vector2");
        VectorDebugger.UpdateColor("vector2", Color.green);

        VectorDebugger.AddVectorsSecuence(vecEjer3, false, "vector3");
        VectorDebugger.UpdateColor("vector3", Color.green);
    }

    private void Update()
    {
        switch (ejercicio)
        {
            case Ejercicios.uno:
                vecEjer1 = QuaternionAux.Euler(0, angle, 0) * vecEjer1;
                SetEditorView("vector1", "vector2", "vector3");
                VectorDebugger.UpdatePosition("vector1", vecEjer1);
                break;

            case Ejercicios.dos:
                for (int i = 0; i < vecEjer2.Count; i++)
                    vecEjer2[i] = QuaternionAux.Euler(0, angle, 0) * vecEjer2[i];
                SetEditorView("vector2", "vector1", "vector3");
                VectorDebugger.UpdatePositionsSecuence("vector2", vecEjer2);
                break;

            case Ejercicios.tres:
                vecEjer3[1] = QuaternionAux.Euler(angle, angle, 0) * vecEjer3[1];
                vecEjer3[3] = QuaternionAux.Euler(-angle, -angle, 0) * vecEjer3[3];
                SetEditorView("vector3", "vector1", "vector2");
                VectorDebugger.UpdatePositionsSecuence("vector3", vecEjer3);
                break;

            default:
                break;
        }
    }

    private void SetEditorView(string on, string off, string off2)
    {
        VectorDebugger.EnableEditorView(on);
        VectorDebugger.DisableEditorView(off);
        VectorDebugger.DisableEditorView(off2);
    }
}