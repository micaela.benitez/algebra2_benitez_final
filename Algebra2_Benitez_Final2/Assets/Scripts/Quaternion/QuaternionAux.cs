﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace CustomMath
{
    public class QuaternionAux : IEquatable<QuaternionAux>
    {
        #region Variables
        public float x;
        public float y;
        public float z;
        public float w;

        const float radToDeg = (180.0f / Mathf.PI);
        const float degToRad = (Mathf.PI / 180.0f);

        // Devuelve el cuaternión con una magnitud de 1
        public QuaternionAux normalized { get { return Normalize(this); } }

        // Accede a los componentes x, y, z, w usando [0], [1], [2], [3] respectivamente
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    case 3: return w;
                    default:
                        throw new IndexOutOfRangeException("Invalid quaternion index");
                }
            }
            set
            {
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    case 3: w = value; break;
                    default:
                        throw new IndexOutOfRangeException("Invalid quaternion index");
                }
            }
        }
        #endregion

        #region constants
        public const float kEpsilon = 1E-06F;
        #endregion

        #region Default Values
        /// Quaternion identity no rota nada
        public static QuaternionAux identity { get { return new QuaternionAux(0.0f, 0.0f, 0.0f, 1.0f); } }
        #endregion

        #region Constructor
        public QuaternionAux(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        public QuaternionAux(QuaternionAux q)
        {
            x = q.x;
            y = q.y;
            z = q.z;
            w = q.w;
        }
        #endregion

        #region Operators
        /// Dado un vector, puede calcular su posición después de aplicar una rotación con la multiplicacion
        public static Vector3 operator *(QuaternionAux rotation, Vector3 point)
        {
            float x = rotation.x * 2.0F;
            float y = rotation.y * 2.0F;
            float z = rotation.z * 2.0F;
            float xx = rotation.x * x;
            float yy = rotation.y * y;
            float zz = rotation.z * z;
            float xy = rotation.x * y;
            float xz = rotation.x * z;
            float yz = rotation.y * z;
            float wx = rotation.w * x;
            float wy = rotation.w * y;
            float wz = rotation.w * z;

            Vector3 res;
            res.x = (1F - (yy + zz)) * point.x + (xy - wz) * point.y + (xz + wy) * point.z;
            res.y = (xy + wz) * point.x + (1F - (xx + zz)) * point.y + (yz - wx) * point.z;
            res.z = (xz - wy) * point.x + (yz + wx) * point.y + (1F - (xx + yy)) * point.z;
            return res;
        }

        /// Combinar rotaciones con la multiplicacion
        public static QuaternionAux operator *(QuaternionAux lhs, QuaternionAux rhs)
        {
            return new QuaternionAux
            (
                lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y,
                lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
                lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
                lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z
            );
        }


        public static bool operator ==(QuaternionAux lhs, QuaternionAux rhs)
        {
            return (lhs == rhs);
        }


        public static bool operator !=(QuaternionAux lhs, QuaternionAux rhs)
        {
            return !(lhs == rhs);
        }
        #endregion

        #region Functions
        /// Devuelve el ángulo en grados entre dos rotaciones a y b
        public static float Angle(QuaternionAux a, QuaternionAux b)
        {
            float f = Dot(a, b);
            return Mathf.Acos(Mathf.Min(Mathf.Abs(f), 1f)) * 2f * radToDeg;
        }

        /// Crea una rotacion que rota tantos (angle) grados alrededor del eje (axis)
        public static QuaternionAux AngleAxis(float angle, Vector3 axis)
        {
            if (axis.sqrMagnitude == 0)
                return identity;

            QuaternionAux result = identity;
            var radians = angle * degToRad;  
            radians *= 0.5f;

            axis.Normalize();
            axis = axis * Mathf.Sin(radians);
            result.x = axis.x;
            result.y = axis.y;
            result.z = axis.z;
            result.w = Mathf.Cos(radians);

            return Normalize(result);
        }

        /// El producto punto/escalar entre dos rotaciones
        public static float Dot(QuaternionAux a, QuaternionAux b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        /// Devuelve una rotación que gira Z grados alrededor del eje Z, X grados alrededor del eje X e Y grados alrededor del eje Y (aplicado en ese orden)
        public static QuaternionAux Euler(Vector3 euler)
        {
            float xCos = Mathf.Cos(Mathf.Deg2Rad * euler.x * 0.5f);
            float xSin = Mathf.Sin(Mathf.Deg2Rad * euler.x * 0.5f);
            float yCos = Mathf.Cos(Mathf.Deg2Rad * euler.y * 0.5f);
            float ySin = Mathf.Sin(Mathf.Deg2Rad * euler.y * 0.5f);
            float zCos = Mathf.Cos(Mathf.Deg2Rad * euler.z * 0.5f);
            float zSin = Mathf.Sin(Mathf.Deg2Rad * euler.z * 0.5f);

            QuaternionAux quaternion = QuaternionAux.identity;
            quaternion.w = xCos * yCos * zCos + xSin * ySin * zSin;
            quaternion.x = xSin * yCos * zCos - xCos * ySin * zSin;
            quaternion.y = xCos * ySin * zCos + xSin * yCos * zSin;
            quaternion.z = xCos * yCos * zSin - xSin * ySin * zCos;

            return quaternion;
        }
        public static QuaternionAux Euler(float x, float y, float z)
        {
            return Euler(new Vector3(x, y, z));
        }

        /// Crea una rotación que gira de fromDirection a toDirection
        public static QuaternionAux FromToRotation(Vector3 fromDirection, Vector3 toDirection)
        {
            return RotateTowards(LookRotation(fromDirection), LookRotation(toDirection), float.MaxValue);
        }

        /// Devuelve el inverso de la rotación
        public static QuaternionAux Inverse(QuaternionAux rotation)
        {
            float lengthSq = rotation.x * rotation.x + rotation.y * rotation.y + rotation.z * rotation.z + rotation.w * rotation.w;
            if (lengthSq != 0.0)
            {
                float i = 1.0f / lengthSq;
                Vector3 vec = new Vector3(rotation.x, rotation.y, rotation.z) * -i;
                return new QuaternionAux(vec.x, vec.y, vec.z, rotation.w * i);
            }
            return rotation;
        }

        /// Interpola entre a y b por t y normaliza el resultado. El parámetro t está sujeto al rango [0, 1]
        public static QuaternionAux Lerp(QuaternionAux a, QuaternionAux b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            return LerpUnclamped(a, b, t);
        }

        /// Interpola entre a y b por t y normaliza el resultado. El parámetro t no esta sujeto a ningun rango
        public static QuaternionAux LerpUnclamped(QuaternionAux a, QuaternionAux b, float t)
        {
            QuaternionAux resultInterpolated = identity;

            if (t >= 1)
                resultInterpolated = b;
            else if (t <= 0)
                resultInterpolated = a;
            else
            {
                QuaternionAux difference = new QuaternionAux(b.x - a.x, b.y - a.y, b.z - a.z, b.w - b.w);
                QuaternionAux differenceLerped = new QuaternionAux(difference.x * t, difference.y * t, difference.z * t, difference.w * t);

                resultInterpolated = new QuaternionAux(a.x + differenceLerped.x, a.y + differenceLerped.y, a.z + differenceLerped.z, a.w + differenceLerped.w);
            }
            return resultInterpolated.normalized;
        }
        
        /// Crea una rotación con las direcciones hacia adelante y hacia arriba especificadas
        public static QuaternionAux LookRotation(Vector3 forward)
        {
            return LookRotation(forward, Vector3.up);
        }
        public static QuaternionAux LookRotation(Vector3 forward, [DefaultValue("Vector3.up")] Vector3 upwards)
        {
            forward = Vector3.Normalize(forward);                                 /// 0, 0, 1
            Vector3 right = Vector3.Normalize(Vector3.Cross(upwards, forward));   /// 1, 0, 0
            upwards = Vector3.Cross(forward, right);                              /// 0, 1, 0 

            float m00 = right.x;
            float m01 = right.y;
            float m02 = right.z;
            float m10 = upwards.x;
            float m11 = upwards.y;
            float m12 = upwards.z;
            float m20 = forward.x;
            float m21 = forward.y;
            float m22 = forward.z;

            float diagonal = m00 + m11 + m22;
            float qw = 0;
            float qx = 0;
            float qy = 0;
            float qz = 0;

            if (diagonal > 0)
            {
                float wComponent = Mathf.Sqrt(diagonal + 1.0f) * 2;
                qw = 0.25f * wComponent;
                qx = (m21 - m12) / wComponent;
                qy = (m02 - m20) / wComponent;
                qz = (m10 - m01) / wComponent;
            }
            else if ((m00 > m11) && (m00 > m22))
            {
                float wComponent = Mathf.Sqrt(1.0f + m00 - m11 - m22) * 2;
                qw = (m21 - m12) / wComponent;
                qx = 0.25f * wComponent;
                qy = (m01 + m10) / wComponent;
                qz = (m02 + m20) / wComponent;
            }
            else if (m11 > m22)
            {
                float wComponent = Mathf.Sqrt(1.0f + m11 - m00 - m22) * 2;
                qw = (m02 - m20) / wComponent;
                qx = (m01 + m10) / wComponent;
                qy = 0.25f * wComponent;
                qz = (m12 + m21) / wComponent;
            }
            else
            {
                float wComponent = Mathf.Sqrt(1.0f + m22 - m00 - m11) * 2;
                qw = (m10 - m01) / wComponent;
                qx = (m02 + m20) / wComponent;
                qy = (m12 + m21) / wComponent;
                qz = 0.25f * wComponent;
            }

            return new QuaternionAux(qx, qy, qz, qw);
        }

        /// Convierte este cuaternión en uno con la misma orientación pero con una magnitud de 1
        public static QuaternionAux Normalize(QuaternionAux q)
        {
            float magnitude = Mathf.Sqrt(Dot(q, q));

            if (magnitude < Mathf.Epsilon)
                return identity;

            return new QuaternionAux(q.x / magnitude, q.y / magnitude, q.z / magnitude, q.w / magnitude);
        }

        /// Realiza una rotación desde from hasta to. El cuaternion from va a girar hacia el cuaternion to con un paso angular de maxDegreesDelta (la 
        /// rotacion no se sobrepasara). Los valores negativos de maxDegreesDelta se alejaran de to hasta que la rotacion este en la direccion opuesta
        public static QuaternionAux RotateTowards(QuaternionAux from, QuaternionAux to, float maxDegreesDelta)
        {
            float angle = Angle(from, to);
            if (angle == 0.0f) return to;
            return SlerpUnclamped(from, to, Mathf.Min(1.0f, maxDegreesDelta / angle));
        }

        /// Igual que lerp pero interpola esfericamente. El parámetro t está sujeto al rango [0, 1]
        public static QuaternionAux Slerp(QuaternionAux a, QuaternionAux b, float t)
        {
            Mathf.Clamp(t, 0, 1f);
            return SlerpUnclamped(a, b, t);
        }

        /// Igual que lerp unclamped pero interpola esfericamente. El parámetro t no esta sujeto a ningun rango
        public static QuaternionAux SlerpUnclamped(QuaternionAux a, QuaternionAux b, float t)
        {
            QuaternionAux quaternion = QuaternionAux.identity;

            // Calcular el ángulo entre ellos
            float cosHalfTheta = a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;

            // Si a=b o a=-b entonces theta = 0 y podemos devolver a
            if (Mathf.Abs(cosHalfTheta) >= 1.0) 
            {
                quaternion.w = a.w; 
                quaternion.x = a.x; 
                quaternion.y = a.y; 
                quaternion.z = a.z;
                return quaternion;
            }

            // Calcular valores temporales
            float halfTheta = Mathf.Acos(cosHalfTheta);
            float sinHalfTheta = Mathf.Sqrt(1.0f - cosHalfTheta * cosHalfTheta);

            // Si theta = 180 grados, el resultado no está completamente definido,
            // podríamos rotar alrededor de cualquier eje normal a o b
            if (Mathf.Abs(sinHalfTheta) < 0.001)
            {
                quaternion.w = (a.w * 0.5f + b.w * 0.5f);
                quaternion.x = (a.x * 0.5f + b.x * 0.5f);
                quaternion.y = (a.y * 0.5f + b.y * 0.5f);
                quaternion.z = (a.z * 0.5f + b.z * 0.5f);
                return quaternion;
            }
            float ratioA = Mathf.Sin((1 - t) * halfTheta) / sinHalfTheta;
            float ratioB = Mathf.Sin(t * halfTheta) / sinHalfTheta;

            //Calcular el cuaternión
            quaternion.w = (a.w * ratioA + b.w * ratioB);
            quaternion.x = (a.x * ratioA + b.x * ratioB);
            quaternion.y = (a.y * ratioA + b.y * ratioB);
            quaternion.z = (a.z * ratioA + b.z * ratioB);
            return quaternion;
        }
        #endregion

        #region Internals
        public bool Equals(QuaternionAux other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);
        }
        public override bool Equals(object other)
        {
            if (!(other is QuaternionAux)) return false;
            return Equals((QuaternionAux)other);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2) ^ (w.GetHashCode() >> 1);
        }

        public void Normalize()
        {
            QuaternionAux quaternion = this;
            quaternion = Normalize(this);
        }

        /// Establece los componentes x, y, z y w de un cuaternion existente
        /// A un cuaternion no se le suma una rotacion, es algo que se setea, por eso no tiene + y -
        public void Set(float newX, float newY, float newZ, float newW)
        {
            x = newX;
            y = newY;
            z = newZ;
            w = newW;
        }

        /// Crea una rotación que gira de fromDirection a toDirection
        public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection)
        {
            QuaternionAux quaternion = this;
            quaternion = FromToRotation(fromDirection, toDirection);
        }

        /// Crea una rotación con las direcciones hacia adelante y hacia arriba especificadas
        public void SetLookRotation(Vector3 view)
        {
            SetLookRotation(view, Vector3.up);
        }
        public void SetLookRotation(Vector3 view, [DefaultValue("Vector3.up")] Vector3 up)
        {
            QuaternionAux quaternion = this;
            quaternion = LookRotation(view, up);
        }

        /// Convierte una rotación en una representación de eje de ángulo (ángulos en grados)
        public void ToAngleAxis(out float angle, out Vector3 axis)
        {
            if (Math.Abs(w) > 1.0f)
                Normalize();

            angle = 2.0f * Mathf.Acos(w);
            float den = Mathf.Sqrt(1.0f - w * w);
            if (den > 0.0001f) axis = new Vector3(x, y, z) / den;
            else axis = new Vector3(1, 0, 0);

            angle *= radToDeg;
        }

        /// Devuelve una cadena formateada del cuaternion
        public string ToString(string format)
        {
            return "X = " + x.ToString(format) + " Y = " + y.ToString(format) + " Z = " + z.ToString(format) + " W = " + w.ToString(format);
        }
        public override string ToString()
        {
            return "X = " + x.ToString() + " Y = " + y.ToString() + " Z = " + z.ToString() + " W = " + w.ToString();
        }
        #endregion
    }
}