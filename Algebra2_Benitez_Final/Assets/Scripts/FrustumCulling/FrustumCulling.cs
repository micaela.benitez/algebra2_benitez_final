﻿using System;
using UnityEngine;
using CustomMath;

namespace FrustumCullingScript
{
    /// Primero calculo el near plane, luego el far plane y a partir de ellos dos, calculo todos los demas
    /// para formar el frustum culling.
    /// Planos del frustum: near - far - right - left - top - bottom

    public class FrustumCulling : MonoBehaviour
    {
        public float rotationSpeed = 10;
        public float rotationX;
        public float rotationY;
        public float rotationZ;

        private Camera frustumCamera;

        [NonSerialized]
        public Plane nearPlane;
        public Plane farPlane;
        public Plane leftPlane;
        public Plane rightPlane;
        public Plane topPlane;
        public Plane bottomPlane;

        private Vector3 position;
        private Vector3 direction;
        private Vector3 up;
        private Vector3 right;
        private Vector3 farCenter;
        private Vector3 nearCenter;
        private float nearDist;
        private float heightNear;
        private float widthNear;
        private float farDist;
        private float heightFar;
        private float widthFar;

        private static FrustumCulling instance;
        public static FrustumCulling Get()
        {
            return instance;
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            DontDestroyOnLoad(gameObject);

            frustumCamera = GetComponent<Camera>();
        }

        private void FixedUpdate()
        {
            transform.Rotate(new Vec3(rotationX, rotationY, rotationZ) * rotationSpeed * Time.deltaTime);
        }

        private void Update()
        {
            NearPlaneDimensions();
            FarPlaneDimensions(); 
            FrustumVariables();
            FrustumPlanes();
        }

        void NearPlaneDimensions()
        {
            float length = frustumCamera.nearClipPlane;
            float angle = frustumCamera.fieldOfView / 2;
            angle = angle * Mathf.Deg2Rad;
            heightNear = (Mathf.Tan(angle) * length);
            widthNear = (heightNear / frustumCamera.pixelHeight) * frustumCamera.pixelWidth;
        }

        void FarPlaneDimensions()
        {
            float length = frustumCamera.farClipPlane;
            float angle = frustumCamera.fieldOfView / 2;
            angle = angle * Mathf.Deg2Rad;
            heightFar = (Mathf.Tan(angle) * length);
            widthFar = (heightFar / frustumCamera.pixelHeight) * frustumCamera.pixelWidth;
        }

        void FrustumVariables()
        {
            position = frustumCamera.transform.position;
            direction = frustumCamera.transform.forward;
            up = frustumCamera.transform.up;
            right = frustumCamera.transform.right;

            nearDist = frustumCamera.nearClipPlane;
            farDist = frustumCamera.farClipPlane;

            farCenter = position + direction * farDist;
            nearCenter = position + direction * nearDist;
        }

        void FrustumPlanes()
        {
            Vector3 nearTopLeft;
            Vector3 nearTopRight;
            Vector3 nearBottomLeft;
            Vector3 nearBottomRight;
            Vector3 farTopLeft;
            Vector3 farTopRight;
            Vector3 farBottomLeft;
            Vector3 farBottomRight;

            nearTopLeft = nearCenter + (up * heightNear) - (right * widthNear);
            nearTopRight = nearCenter + (up * heightNear) + (right * widthNear);
            nearBottomLeft = nearCenter - (up * heightNear) - (right * widthNear);
            nearBottomRight = nearCenter - (up * heightNear) + (right * widthNear);
            farTopLeft = farCenter + (up * heightFar) - (right * widthFar);
            farTopRight = farCenter + (up * heightFar) + (right * widthFar);
            farBottomLeft = farCenter - (up * heightFar) - (right * widthFar);
            farBottomRight = farCenter - (up * heightFar) + (right * widthFar);

            // Near/Far plane
            nearPlane = new Plane(nearTopLeft, nearTopRight, nearBottomLeft);
            farPlane = new Plane(farTopRight, farTopLeft, farBottomLeft);

            // Frustum view
            leftPlane = new Plane(position, farBottomLeft, farTopLeft);
            rightPlane = new Plane(position, farTopRight, farBottomRight);
            topPlane = new Plane(position, farTopLeft, farTopRight);
            bottomPlane = new Plane(position, farBottomRight, farBottomLeft);

            nearPlane.Flip();
            farPlane.Flip();
            leftPlane.Flip();
            rightPlane.Flip();
            topPlane.Flip();
            bottomPlane.Flip();
        }
    }
}